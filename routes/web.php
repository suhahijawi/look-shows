<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/episode/{seriesId}/{id}', 'EpisodesController@Single')->name('SingleEpisode');
Route::get('/like/{id}', 'EpisodesController@LikeEpisode')->name('LikeEpisode');

Route::get('/series/{id}', 'SeriesController@Single')->name('SingleSeries');
Route::get('/follow/{id}', 'SeriesController@FollowSeries')->name('FollowSeries');


route::get('/search','HomeController@search')->name('search');

Route::prefix('admin')->group(function () {
    Route::get('/dashboard/', 'admin\DashboardController@index')->name('adminPanel');
    Route::resource('users', 'admin\UsersController');
    Route::resource('series', 'admin\SeriesController');
    Route::resource('episodes', 'admin\EpisodesController');
});
