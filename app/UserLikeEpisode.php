<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLikeEpisode extends Model
{
    protected $table = 'users_likes_episodes';
    protected $fillable = ['users_id', 'episodes_episodeID'];
    protected $primaryKey = 'id';


    public static function addLike($userId, $episodeId)
    {
        $Liked = UserLikeEpisode::CheckifLiked($userId, $episodeId);
        if ($Liked > 0)

            return UserLikeEpisode::RemoveLike($userId, $episodeId);


        $like = new UserLikeEpisode();
        $like->users_id = $userId;
        $like->episodes_episodeID = $episodeId;
        $like->save();
        return $like;
    }

    public static function EpisodeLikes($episodeId)
    {
        $Count = UserLikeEpisode::where(['episodes_episodeID' => $episodeId])->count();
        return $Count;
    }

    public static function CheckifLiked($userId, $episodeId)
    {
        $Count = UserLikeEpisode::where(['episodes_episodeID' => $episodeId])
            ->where(['users_id' => $userId])
            ->count();
        return $Count;
    }

    public static function RemoveLike($userId, $episodeId)
    {
        UserLikeEpisode::where(['episodes_episodeID' => $episodeId])
            ->where(['users_id' => $userId])
            ->delete();
        return false;
    }

}
