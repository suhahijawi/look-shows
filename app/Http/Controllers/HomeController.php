<?php

namespace App\Http\Controllers;

use App\episodes;
use App\series;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $LatestEpisodes = episodes::getLatest();

        return view('home', compact('LatestEpisodes'));
    }

    public function search(request $request)
    {
        Cache::put('search', $request['search'], 3900);
        if (series::searchForSeries($request->all())) {
            $series = series::where('title', 'like', $request['search'])->first();
            return Redirect::route('SingleSeries', $series->seriesID);
        } elseif (episodes::searchForEpisodes($request->all())) {
            $episode = episodes::where('title', 'like', $request['search'])->first();
            return Redirect::route('SingleEpisode', $episode->episodeID);
        } else {
            return Redirect::back()->with('alert_warning', 'There\'s no such series or episode');
        }
    }
}
