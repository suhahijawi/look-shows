<?php

namespace App\Http\Controllers;

use App\episodes;
use App\UserLikeEpisode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EpisodesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Single($seriesId,$id)
    {
        $Episode = episodes::where('episodeID',$id)->where('series_seriesID',$seriesId)->first();
        $Likes=UserLikeEpisode::EpisodeLikes($id);
        $NextEpisodes=episodes::NextEpisodes($Episode->Series->seriesID,$id);
        return view('single_episode', compact('Episode','Likes','NextEpisodes'));
    }

    public function LikeEpisode($id)
    {
        $user_id = Auth::user()->id;
         UserLikeEpisode::addLike($user_id,$id);

        return redirect()->back();
    }
}
