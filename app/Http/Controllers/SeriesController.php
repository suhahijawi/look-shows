<?php

namespace App\Http\Controllers;

use App\episodes;
use App\series;
use App\UserFollowSeries;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SeriesController extends Controller
{
    public function Single($id)
    {
        $Series = series::find($id);
        $Follows=UserFollowSeries::SeriesFollow($id);
       $SeriesEpisodes=episodes::SeriesEpisodes($id);
        return view('single_series', compact('Series','Follows','SeriesEpisodes'));
    }

    public function FollowSeries($id)
    {
        $user_id = Auth::user()->id;
        UserFollowSeries::addFollow($user_id, $id);
        return redirect()->back();
    }

}
