<?php

namespace App\Http\Controllers\admin;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    protected function validateUsers()
    {
        return $this->validate(Request(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'image' => 'required|image|mimes:jpg,png,jpeg|max:2000'
        ]);
    }

    public  function index()
    {
        $users = User::all();
        return view('admin.users.users-list',compact(['users']));
    }

}
