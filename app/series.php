<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class series extends Model
{
    protected $table = 'series';
    protected $fillable = ['title', 'description', 'AiringTime', 'StartDay', 'EndDay'];
    protected $primaryKey = 'seriesID';

    public function episodes()
    {
        return $this->hasMany('App\episodes');
    }

    public static function storeSeries($request)
    {
        $series = new series;
        $series->title = $request['title'];
        $series->description = $request['description'];
        $series->AiringTime = $request['AiringTime'];
        $series->StartDay = $request['StartDay'];
        $series->EndDay = $request['EndDay'];
        $series->save();
        return $series;
        // returning series because why not
    }

    public static function editSeries($request, $seriesID)
    {
        $series = series::find($seriesID);
        $series->title = $request['title'];
        $series->description = $request['description'];
        $series->AiringTime = $request['AiringTime'];
        $series->StartDay = $request['StartDay'];
        $series->EndDay = $request['EndDay'];
        $series->save();

    }

    public static function deleteSeries($seriesID)
    {
        $series = series::find($seriesID);
        if ($series != null)
            $series->delete();
    }

    public static function searchForSeries($request)
    {
        $series = series::where('title', $request['search'])->first();
        if ($series != null)
            return true;
        else
            return false;
    }

    public static function getRandom($limit)
    {
        $series = series::inRandomOrder()->limit($limit)->get();
        return $series;
    }
}
