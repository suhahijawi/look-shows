<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFollowSeries extends Model
{
    protected $table = 'users_follows_series';
    protected $fillable = ['users_id', 'series_seriesID'];
    protected $primaryKey = 'id';


    public static function addFollow($userId, $seriesId)
    {
        $Followed = UserFollowSeries::CheckifFollwed($userId, $seriesId);
        if ($Followed > 0)
            return UserFollowSeries::RemoveFollow($userId, $seriesId);
        $Follow = new UserFollowSeries();
        $Follow->users_id = $userId;
        $Follow->series_seriesID = $seriesId;
        $Follow->save();
        return $Follow;
    }

    public static function SeriesFollow($seriesId)
    {
        $Count = UserFollowSeries::where(['series_seriesID' => $seriesId])->count();
        return $Count;
    }

    public static function CheckifFollwed($userId, $seriesId)
    {
        $Count = UserFollowSeries::where(['series_seriesID' => $seriesId])
            ->where(['users_id' => $userId])
            ->count();
        return $Count;
    }

    public static function RemoveFollow($userId, $seriesId)
    {
        UserFollowSeries::where(['series_seriesID' => $seriesId])
            ->where(['users_id' => $userId])
            ->delete();
        return false;
    }
}
