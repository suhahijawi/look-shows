@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="col-md-12">
            <h1 class=" col-xs-12">{{$Series->title}} </h1>

            <div class="box">
                <a href="{{route('FollowSeries',['id'=>$Series->seriesID])}}"
                   class="float-right follow-btn">{{$Follows}}
                    Follow <i class="fa fa-plus-square"></i></a>
            </div>
            <div class="clearfix"></div>
            <div class="box-body">

                <p>Show Time : {{date('l',strtotime($Series->StartDay))}}
                    - {{date('l',strtotime($Series->EndDay))}} {{date('@ H:m',strtotime($Series->AiringTime))}}</p>
                <p>{{$Series->description}}</p>
            </div>
        </div>

        @if(sizeof($SeriesEpisodes)>0)

            <h4 class="col-xs-12">حلقات البرنامج</h4>
            @foreach($SeriesEpisodes as $item)
                <div class="col-md-4">
                    <div class="card">
                        <a href="{{route('SingleEpisode',['seriesId'=>$item->series_seriesID,'id'=>$item->episodeID])}}">
                            <div class="card-body no-padding">
                                <img src="/{{$item->thumbnail}}" width="100%" height="200px">
                                <h2>{{$item->title}} </h2>
                                <small><b>{{date('d-M-Y',strtotime($item->AiringTime))}}</b></small>
                                <p>{{$item->description}}</p>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach

        @endif
    </div>
@endsection
