@extends('layouts.app')

@section('content')
    <div class="container">

        <h1 class=" col-xs-12">أحدث الحلقات </h1>
        @foreach($LatestEpisodes as $item)
            <div class="col-md-4">
                <div class="card">
                   <a href="{{route('SingleEpisode',['seriesId'=>$item->series_seriesID,'id'=>$item->episodeID])}}">
                       <div class="card-body no-padding">
                           <img src="{{$item->thumbnail}}" width="100%" height="200px">
                           <h2>{{$item->title}} </h2>
                           <small><b>{{date('d-M-Y',strtotime($item->AiringTime))}}</b></small>
                           <p>{{$item->description}}</p>
                       </div>
                   </a>
                </div>
            </div>

        @endforeach

    </div>
@endsection
