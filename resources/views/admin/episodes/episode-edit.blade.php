@extends('admin.layouts.app')
@section('content')

        <div class="content-wrapper">
            <div class="panel panel-default">
                <div class="panel-header"><h2 class="col-xs-12">Edit Episode</h2></div>
                <div class="panel-body">


                    <div class="row">
                        <div class="col-lg-6">
                            <form method="POST" action="{{route('episodes.update',$episode->episodeID)}}" enctype="multipart/form-data" >
                                @csrf
                                @method('PUT')


                                <div class="form-group">
                                    Title
                                    <input class="form-control" type="text" name="title" value="@if(old('title')){{old('title')}}@else{{$episode->title}}@endif">
                                    @if ($errors->has('title'))
                                        <span >
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    Description
                                    <textarea class="form-control" name="description" >@if(old('description')){{old('description')}}@else{{$episode->description}}@endif</textarea>
                                    @if ($errors->has('description'))
                                        <span >
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    Episode series
                                    <select class="form-control" name="series_seriesID" >
                                        <option disabled value="">Please select series</option>
                                        @foreach($series AS $item)
                                            <option @if($episode->series_seriesID == $item->seriesID) {{"SELECTED"}} @endif value="{{$item->seriesID}}">{{$item->title}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('series_seriesID'))
                                        <span>
                                            <strong>{{ $errors->first('series_seriesID') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>  Airing time</label>
                                    <input class="form-control" type="datetime-local" name="AiringTime" value="@if(old('AiringTime')){{old('AiringTime')}}@else{{$episode->AiringTime}}@endif">
                                    @if ($errors->has('AiringTime'))
                                        <span >
                                                    <strong>{{ $errors->first('AiringTime') }}</strong>
                                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>  Duration</label>
                                    <input class="form-control" type="time" name="duration" value="@if(old('duration')){{old('duration')}}@else{{$episode->duration}}@endif">
                                    @if ($errors->has('duration'))
                                        <span >
                                                    <strong>{{ $errors->first('duration') }}</strong>
                                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    Thumbnail

                                        <input class="form-control" type="file" name="thumbnail" value="@if(old('thumbnail')){{old('thumbnail')}}@else{{$episode->thumbnail}}@endif">

                                        <img  class="img-circle" width="50px" height="50px" src="/@if(old('thumbnail')){{old('thumbnail')}}@else{{$episode->thumbnail}}@endif">

                                    @if ($errors->has('thumbnail'))
                                        <span >
                                            <strong>{{ $errors->first('thumbnail') }}</strong>
                                        </span>
                                    @endif
                                </div>


                                <div class="form-group">
                                    Video
                                    <input class="form-control" type="text" name="video" value="@if(old('video')){{old('video')}}@else{{$episode->video}}@endif">
                                    <iframe width="200" height="200"
                                            src="@if(old('video')){{old('video')}}@else{{str_replace('watch?v=','embed/',$episode->video)}}@endif">
                                    </iframe>


                                @if ($errors->has('video'))
                                        <span>
                                            <strong>{{ $errors->first('video') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div>
                                    <button type="submit" class="btn btn-default">Update</button>
                                </div>
                            </form>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->

        </div>
  
@endsection

