@extends('admin.layouts.app')
@section('content')

        <div class="content-wrapper">
            <div class="panel panel-default">
                <div class="panel-header"><h2 class="col-xs-12">View Episode</h2></div>
                <div class="panel-body">


                    <div class="row">
                        <div class="col-lg-6">
                            <form >


                                <div class="form-group">
                                    Title
                                    <input disabled class="form-control" type="text" value=" {{$episode->title}}">

                                </div>
                                <div class="form-group">
                                    Series Title
                                    <input disabled class="form-control" type="text" value=" {{$episode->Series->title}}">

                                </div>
                                <div class="form-group">
                                    Airing Time
                                    <input disabled class="form-control" type="text" value=" {{$episode->AiringTime}}">

                                </div>
                                <div class="form-group">
                                    Duration
                                    <input disabled class="form-control" type="text" value=" {{$episode->duration}}">

                                </div>
                                <div class="form-group">
                                    Description
                                    <textarea disabled class="form-control" > {{$episode->description}}</textarea>
                                </div>

                                <div class="form-group">
                                    <img class="img-circle" src="/{{$episode->thumbnail}}" width="100" height="100">
                                </div>
                                <iframe width="200" height="200"
                                        src="@if(old('video')){{old('video')}}@else{{str_replace('watch?v=','embed/',$episode->video)}}@endif">
                                </iframe>

                            </form>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->

        </div>


@endsection
