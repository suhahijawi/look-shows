@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Episodes List
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('adminPanel')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a class="active" href="/admin/episodes">Episodes</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Episode Id</th>
                                    <th>Thumbnail</th>
                                    <th>Title</th>
                                    <th>Airing Time</th>
                                    <th>Duration</th>
                                    <th>Series Title</th>
                                    <th>Control</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($episodes as $item)

                                    <tr>
                                        <td>{{$item->episodeID}}</td>
                                        <td><img  class="img-circle" width="50px" height="50px"src="../{{$item->thumbnail}}"></td>

                                        <td>{{$item->title}}</td>
                                        <td>{{$item->AiringTime}}</td>
                                        <td>{{$item->duration}}</td>
                                        <td>{{$item->Series->title}}</td>
                                        <td>
                                            <a  href="{{route('episodes.edit',$item->episodeID)}}" >Edit | </a>
                                            <a href="{{route('episodes.show',$item->episodeID)}}" >View</a>
                                            <form method="POST" action="{{route('episodes.destroy',$item->episodeID)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('Are you sure?')"  >Delete</button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection


