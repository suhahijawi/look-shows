@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users List
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('adminPanel')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a class="active" href="users">Users</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>User Id</th>
                                    <th>Image</th>
                                    <th>User Name</th>

                                    <th>Email</th>

                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($users as $user)

                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td><img class="img-circle" width="50px" height="50px"
                                                 src="../{{$user->image}}"></td>
                                        <td>{{$user->name}}
                                        </td>

                                        <td> {{$user->email}}</td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection


