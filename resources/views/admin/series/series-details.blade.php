@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">

            <div class="panel panel-default">
                <div class="panel-header"><h2 class="col-xs-12">View series</h2></div>
                <div class="panel-body">


                    <div class="row">
                        <div class="col-lg-6">
                            <form >


                                <div class="form-group">
                                    Title
                                    <input disabled class="form-control" type="text" value=" {{$series->title}}">

                                </div>
                                <div class="form-group">
                                    Description
                                    <textarea disabled class="form-control" > {{$series->description}}</textarea>
                                </div>

                                <div class="form-group">
                                    Airing time
                                    <input disabled class="form-control" type="text" value="{{$series->AiringTime}}">
                                </div>
                                <div class="form-group">
                                    Start Day
                                    <input disabled class="form-control" type="text" value="{{date('l',strtotime($series->StartDay))}}">
                                </div>
                                <div class="form-group">
                                    End Day
                                    <input disabled class="form-control" type="text" value="{{date('l',strtotime($series->EndDay))}}">
                                </div>
                            </form>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>

    </div>

    @endsection
