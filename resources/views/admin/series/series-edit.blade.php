@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">

            <div class="panel panel-default">
                <div class="panel-header"><h2>Edit series</h2></div>
                <div class="panel-body">


                    <div class="row">
                        <div class="col-lg-6">
                            <form method="POST" action="{{route('series.update',$series->seriesID)}}" >
                                @csrf
                                @method('PUT')


                                <div class="form-group">
                                    Title
                                    <input class="form-control" type="text" name="title" value="@if(old('title')){{old('title')}}@else{{$series->title}}@endif">
                                    @if ($errors->has('title'))
                                        <span >
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    Description
                                    <textarea class="form-control" name="description" >@if(old('description')){{old('description')}}@else{{$series->description}}@endif</textarea>
                                    @if ($errors->has('description'))
                                        <span >
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>  Airing time</label>
                                    <input class="form-control" type="time" name="AiringTime" value="@if(old('AiringTime')){{old('AiringTime')}}@else{{$series->AiringTime}}@endif">
                                    @if ($errors->has('AiringTime'))
                                        <span >
                                                    <strong>{{ $errors->first('AiringTime') }}</strong>
                                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Start Day</label>
                                    <input class="form-control" type="date" name="StartDay" value="@if(old('StartDay')){{old('StartDay')}}@else{{$series->StartDay}}@endif">
                                    @if ($errors->has('StartDay'))
                                        <span >
                                                    <strong>{{ $errors->first('StartDay') }}</strong>
                                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>End Day</label>
                                    <input class="form-control" type="date" name="EndDay" value="@if(old('EndDay')){{old('EndDay')}}@else{{$series->EndDay}}@endif">
                                    @if ($errors->has('EndDay'))
                                        <span >
                                                    <strong>{{ $errors->first('EndDay') }}</strong>
                                                </span>
                                    @endif
                                </div>

                                <div>
                                    <button type="submit" class="btn btn-default">Update</button>
                                </div>
                            </form>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>

    </div>
@endsection

