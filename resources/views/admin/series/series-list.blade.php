@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Series List
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('adminPanel')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a class="active" href="/admin/series">Series</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Series Id</th>
                                    <th>Title</th>
                                    <th>Airing Time</th>
                                    <th>Start Day</th>
                                    <th>End Day</th>
                                    <th>Control</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($series as $item)

                                    <tr>
                                        <td>{{$item->seriesID}}</td>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->AiringTime}}</td>
                                        <td>{{$item->StartDay}}</td>
                                        <td>{{$item->EndDay}}</td>
                                        <td>
                                            <a  href="{{route('series.edit',$item->seriesID)}}" >Edit | </a>
                                            <a href="{{route('series.show',$item->seriesID)}}" >View</a>
                                            <form method="POST" action="{{route('series.destroy',$item->seriesID)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('Are you sure?')"  >Delete</button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection


