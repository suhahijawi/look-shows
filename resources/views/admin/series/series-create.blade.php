@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">

            <div class="panel panel-default">
                <div class="panel-header"><h2 class="col-xs-12">Create series</h2></div>
                <div class="panel-body">


                    <div class="row">
                        <div class="col-lg-6">
                            <form method="POST" action="{{route('series.store')}}" >
                               @csrf


                                        <div class="form-group">
                                           <label> Title</label>
                                            <input class="form-control" type="text" name="title" value="{{old('title')}}">
                                            @if ($errors->has('title'))
                                                <span >
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" name="description" >{{old('description')}}</textarea>
                                            @if ($errors->has('description'))
                                                <span>
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                          <label>  Airing time</label>
                                            <input class="form-control" type="time" name="AiringTime" value="{{old('AiringTime')}}">
                                            @if ($errors->has('AiringTime'))
                                                <span >
                                                    <strong>{{ $errors->first('AiringTime') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                <div class="form-group">
                                    <label>Start Day</label>
                                    <input class="form-control" type="date" name="StartDay" value="{{old('StartDay')}}">
                                    @if ($errors->has('StartDay'))
                                        <span >
                                                    <strong>{{ $errors->first('StartDay') }}</strong>
                                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>End Day</label>
                                    <input class="form-control" type="date" name="EndDay" value="{{old('EndDay')}}">
                                    @if ($errors->has('EndDay'))
                                        <span >
                                                    <strong>{{ $errors->first('EndDay') }}</strong>
                                                </span>
                                    @endif
                                </div>

                                <div>
                                    <button type="submit" class="btn btn-default">Store</button>
                                </div>
                            </form>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>

    </div>
@endsection

