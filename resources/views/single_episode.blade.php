@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="col-md-8">
            <h1 class=" col-xs-12">{{$Episode->Series->title}} | {{$Episode->title}}</h1>
            <iframe width="100%" height="300px"
                    src="{{str_replace('watch?v=', 'embed/', $Episode->video)}}">
            </iframe>
            <div class="box">
               <div class="float-right">
                   <p >   مدة العرض : {{$Episode->duration}} </p>
                   <p> وقت العرض :  {{date('l @ H:m',strtotime($Episode->AiringTime))}}</p>
               </div>
                <a href="{{route('LikeEpisode',['id'=>$Episode->episodeID])}}" class="float-left like-btn"> {{$Likes}}
                    <i class="fas fa-thumbs-up"></i></a>
            </div>
            <div class="clearfix"></div>

            <div class="box-body">
                <p>{{$Episode->description}}</p>
            </div>
        </div>

        @if(sizeof($NextEpisodes)>0)
            <div class="col-md-4">
                <h4>Next Episodes</h4>
                @foreach($NextEpisodes as $item)

                    <div class="card">
                        <a href="{{route('SingleEpisode',['id'=>$item->episodeID])}}">
                            <div class="card-body no-padding">
                                <img src="/{{$item->thumbnail}}" width="100%" height="200px">
                                <h2>{{$item->title}} </h2>
                                <small><b>{{date('d-M-Y',strtotime($item->AiringTime))}}</b></small>
                                <p>{{$item->description}}</p>
                            </div>
                        </a>
                    </div>

                @endforeach
            </div>
        @endif
    </div>
@endsection
